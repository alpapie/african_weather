from datetime import date, datetime
from django.http import HttpResponse
from django.shortcuts import render
from django.http import HttpResponse
import json
import requests
# Create your views here.


def index(request):
    return render(request,'index.html')

def weather(request):
    city=request.POST.get('vil_name')
    data_se=requests.get('https://api.openweathermap.org/data/2.5/forecast?q='+city+'&appid=4d8fb5b93d4af21d66a2948710284366&units=metric&cnt=5')
    data=data_se.json() 
    if(request.POST.get('long')):
        long=request.POST.get('long')
        larg=request.POST.get('larg')
        return HttpResponse(long)
    if(data["cod"]=='200'):
        weather=data['list']
        temps={
            '01d':'un ciel clair',
            '02d':'quelques nuages',
            '03d':'nuages épars',
            '04d':'nuages fragmentés',
            '09d':'douche pluie',
            '10d':'pluie',
            '11d':'orage',
            '13d':'neige',
            '50d':'brouillard',}
        for i in weather:
            i['date']=datetime.strptime(i['dt_txt'], '%Y-%m-%d %H:%M:%S')
            i['icon']='https://openweathermap.org/img/wn/'+i['weather'][0]['icon']+'@2x.png'
            i['temps']=temps.get(i['weather'][0]['icon'])     
        weather0=weather[0]
        del weather[0]
        return render(request,'temp.html',{'data':data,"weather":weather,"weather0":weather0,"temps":temps})
    error='veiller taper une nom de ville valide'
    return render(request,'index.html',{'error':error})