from django import urls
from django.urls import URLPattern, path

from . import views

urlpatterns =[
    path('', views.index, name='index'),
    path('weather',views.weather, name='weather'),
    ]